# datasets

List of datasets (small) managed by me.
1. Let's Dance Pose Estimation Numpy Dataset
https://www.cc.gatech.edu/cpl/projects/dance/
{
  the image frames data are processed with a multi-subject pose estimation model to   extract the coordinates of body joints.
}